﻿# HyFlex

Documents and materials on the [CHESC2011](http://www.asap.cs.nott.ac.uk/external/chesc2011/index.html) website.

## Description
- chesc_benchmark: benchmark calculator and competition results
- Documentation
    - Notes: some notes about how to use HyFlex
    - chesc2011-original-site.zip: the content of the website 
    - literatures about HyFlex and CHESC competition
- Examples: HyFlex examples 
- chesc.jar: HyFlex software downloaded as Java jar file. This JAR file is provided for archival purposes; serious errors relating to the Personnel Scheduling domain are fixed in the most recent download! - 17/03/2019
- chesc-fixed-no-ps.jar: HyFlex Framework with minor bug fixes and the CHeSC problem domains (less Personnel Scheduling) - 17/03/2019
- chesc-ps.jar: HyFlex framework with the corrected Personnel Scheduling problem domain - 17/03/2019
- jd-gui.exe: a standalone graphical utility that displays Java source codes of “.class” files. You can browse the reconstructed source code with the JD-GUI for instant access to methods and fields. (see http://java-decompiler.github.io/)
- winbm.exe: the benchmark executable program (see http://www.asap.cs.nott.ac.uk/external/chesc2011/benchmarking.html)

## Access CHESC2011 website
This website is updated by the School of Computer Science, University of Nottingham. You may need to connect University’s VPN (see https://www.nottingham.ac.uk/dts/communications/remote-working/access-systems.aspx) if you access the CHESC2011 website from off-site.